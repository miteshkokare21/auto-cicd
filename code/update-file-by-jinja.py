def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = value
    return props

def read_file(fileName):
    filedata = None
    file = open( fileName, 'r')
    filedata = file.read()
    file.close()
    return filedata

def update_file(fileName, service,update_fileName):
    template = env.get_template(fileName)
    output = template.render(service=service)
    f = open(update_fileName, "a")
    f.write(output)
    f.close()
    
def update_compose_file(fileName, service,update_fileName):
    template = env.get_template(fileName)
    output = template.render(service=service)
    f = open(update_fileName, "a")
    f.write(output)
    f.close()

def update_stack_file(fileName, service,update_fileName):
    template = env.get_template(fileName)
    append_output = template.render(service=service)
    read_file=[]
    print(update_fileName)
    with open(update_fileName, 'r') as fh:
        for line in fh:
            if line.startswith('networks:'):
                read_file.append(append_output)
                read_file.append("\n")
            read_file.append(line)
    f = open(update_fileName, "w")
    f.write(''.join(read_file))
    f.close()

    
if __name__ == '__main__':
    from jinja2 import Environment, FileSystemLoader
    
    # fetching propety values from groovy file
    service = load_properties('code/project.groovy')
    
    # extracting file name
    file_path = service['file_path']
    file_name = file_path.split("/")
    # = read_file(file_name[-1])

    file_loader = FileSystemLoader('code/templates')
    env = Environment(loader=file_loader)
    if service['deployment_type'] == "stack":
        update_stack_file('update_stack_file', service,'code/'+file_name[-1])
    elif service['deployment_type'] == "compose":
        update_compose_file('update_compose_file', service, 'code/'+file_name[-1])
