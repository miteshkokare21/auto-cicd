import requests , json
url="http://gitlab.com/api/v4/projects" 
jenkins_url="https://0ed4a7f6.ngrok.io/"
jenkins_url_web="https://mitesh:11726ff824c52cd00c359eda32ef00354b@0ed4a7f6.ngrok.io/"
git_headers = {"PRIVATE-TOKEN": "Sy32nvqTsbTN4Tax2hmB"} 
git_headers1 = {"PRIVATE-TOKEN": "Sy32nvqTsbTN4Tax2hmB", "Content-Type": "application/json"}
jenkins_headers={"Content-Type":"text/xml"} 
get_id =0 

def load_properties(filepath, sep='=', comment_char='#'):
    """
    Read the file passed as parameter as a properties file.
    """
    props = {}
    with open(filepath, "rt") as f:
        for line in f:
            l = line.strip()
            if l and not l.startswith(comment_char):
                key_value = l.split(sep)
                key = key_value[0].strip()
                value = sep.join(key_value[1:]).strip().strip('"')
                props[key] = value
    return props 

def fetch_project(git_url):
    split_slash_url = git_url.split('/')
    gitProject = split_slash_url[-1]
    split_dot_url=gitProject.split('.')
    # Fetching gitlab project id
    r = requests.get(url+"/?search="+split_dot_url[0], headers=git_headers)
    return r.content 

def fetch_project_id(git_url):
    datastore = json.loads(fetch_project(git_url))
    for i in datastore:
        if git_url == i['http_url_to_repo']:
            get_id = i['id']
            print("Project Found in Gitlab and id is "+str(get_id))
            return get_id, True
    print("Project not found in GitLab")
    return 0, False

def read_file(fileName):
    filedata = None
    file = open( fileName, 'r')
    filedata = file.read()
    file.close()
    return filedata

def generate_config(git_url,git_branch):
    # Reading code/templates/config_template.xml file
    filedata = read_file("code/templates/config_template.xml")
    # Replace the target string
    newdata = filedata.replace('@git_url@', git_url)
    filedata = newdata.replace('@git_branch@', git_branch)
    return filedata

def create_job(filedata,jobname):
    ##Creating jenkins job
    r = requests.post(jenkins_url_web+"createItem?name="+jobname, data=filedata, headers=jenkins_headers)
    print("Jenkins job "+jobname+" created")
	
def check_job_exist(filedata,jobname):
    ##Creating jenkins job
    r = requests.post(jenkins_url_web+"job/"+jobname+"/api/json", headers=jenkins_headers)
    if r.status_code == 200:
        print("Project exist in "+jobname+" jenkins")
        return False
    create_job(filedata,jobname)
    return True

def check_webhook_exist(get_id,jobname):
    # Creating Webhook
    r = requests.get(url+"/"+str(get_id)+"/hooks", headers=git_headers)
    print(url+"/"+str(get_id)+"/hooks")
    #print(r.status_code, r.reason)
    datastore = json.loads(r.content)
    for i in datastore:
        print(i['url'])
        if jenkins_url+jobname == i['url']:
            get_id = i['id']
            print("Already Webhook exist")
            return
    create_webhook(get_id, jobname) 

def create_webhook(get_id,jobname):
    # Creating Webhook
    print("Creating new webhook")
    r = requests.post(url+"/"+str(get_id)+"/hooks?push_events=True&enable_ssl_verification=False&url="+jenkins_url+jobname, headers=git_headers)

def check_file_available(get_id, branch,commit_message, file_path):
    content=read_file(file_path)
    action="create"
    r = requests.post(url+"/"+str(get_id)+"/repository/commits?branch="+branch+"&commit_message="+commit_message+"&actions[][action]="+action+"&actions[][file_path]="+file_path+"&actions[][content]="+content.encode('utf-8'), headers=git_headers)
    if r.status_code != 200:
        action="update"
        r = requests.post(url+"/"+str(get_id)+"/repository/commits?branch="+branch+"&commit_message="+commit_message+"&actions[][action]="+action+"&actions[][file_path]="+file_path+"&actions[][content]="+content.encode('utf-8'), headers=git_headers)

def check_file_available1(get_id, branch,commit_message, file_path, file_name):
    #https://docs.gitlab.com/ee/api/repository_files.html
    data={"branch": branch, "author_email": "miteshkokare21@gmail.com", "author_name": "Mitesh Kokare", "content": read_file(file_path).encode('utf-8'), "commit_message": "Creating new file"}
    r = requests.post(url=url+"/"+str(get_id)+"/repository/files/"+file_name, data=json.dumps(data),headers=git_headers1)
    if r.status_code == 400:
       data={"branch": branch, "author_email": "miteshkokare21@gmail.com", "author_name": "Mitesh Kokare", "content": read_file(file_path).encode('utf-8'), "commit_message": "Updating file"}
       r = requests.put(url=url+"/"+str(get_id)+"/repository/files/"+file_name, data=json.dumps(data),headers=git_headers1)
       print("Updating "+file_path+" file in git")
       return
    print("Creating "+file_path+" file in git")
    return

if __name__ == '__main__':
    props = load_properties('code/project.groovy')
    get_id, bool = fetch_project_id(props['git_url'])
    if bool == True:
        filedata=generate_config(props['git_url'],props['git_branch'])
        ## Passing Job Name in argument
        if check_job_exist(filedata,props['image_name']) ==True:
            check_file_available1(get_id, props['git_branch'], "Updating File", 'code/project.groovy', 'project%2Egroovy')
            check_file_available1(get_id, props['git_branch'], "Updating File", 'code/Jenkinsfile' , 'Jenkinsfile')
            check_webhook_exist(get_id, props['image_name'])## Passing Job Name in argument

